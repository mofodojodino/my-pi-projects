#!/usr/bin/python
#
# photocell.py
# demo for photocell module to time charge time
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
import time
import sys

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

#
# measure the charge time of of a 
# photocell
#
def RCTime(PiPin) :

    measurement = 0
    # discharge capacitor
    GPIO.setup(PiPin, GPIO.OUT)
    GPIO.output(PiPin, GPIO.LOW)
    time.sleep(0.1)

    GPIO.setup(PiPin, GPIO.IN)

    # Count loops until voltage across
    # capacitor reads high on GPIO
    while (GPIO.input(PiPin) == GPIO.LOW):
        measurement += 1

    return measurement

try : 

    # Loop until users quits with CTRL-C
    while True :
        #print(RCTime(25))
        sys.stdout.write("\r    " + str(RCTime(25)) + "   ")
        sys.stdout.flush()
        
        # Wait for 10 milliseconds
        time.sleep(0.1)

except KeyboardInterrupt :
    print("  Quit")
    # Reset GPIO settings
    GPIO.cleanup()