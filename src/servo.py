#!/usr/bin/python
#
# servo.py
# Test servo in continuous rotation mode
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
from RPIO import PWM
import time
import sys

PIN = 18

servo = PWM.Servo()

# Set servo on GPIO17 to 1200us (1.2ms)
servo.set_servo(PIN, 1640)
time.sleep(5)
servo.stop_servo(PIN)

#servo.set_servo(PIN, 2670)
servo.set_servo(PIN, 2730)
#servo.set_servo(PIN, 550)

time.sleep(3.03)

# Clear servo on GPIO17
servo.stop_servo(PIN)

# Set servo on GPIO17 to 2000us (2.0ms)
#servo.set_servo(PIN, 1000)
#time.sleep(3.0)

#servo.set_servo(PIN, 1500)

# Clear servo on GPIO17
#servo.stop_servo(PIN)