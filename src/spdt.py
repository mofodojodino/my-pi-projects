#!/usr/bin/python
#
# spdt.py
# Test a switch by outputing state change when activated.
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
import time
import sys

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

class Switch :

    def __init__(self, pin) :
        self.pin = pin
        self.setup()
        print("Switch on GPIO pin " + str(pin))
        
    #
    # Setup pin as a input sensor
    #
    def setup(self) :
        GPIO.setup(self.pin, GPIO.IN, GPIO.PUD_UP)

    #
    # get current state of sensor
    #
    def state(self) :
        # pdb.set_trace()
        return GPIO.input(self.pin)

    #
    # test if sensor is active
    #
    def isActive(self) :
        return True if self.state() == GPIO.LOW else False
        
PIN = 17

try : 
    
    switch = Switch(PIN)
        
    # Loop until users quits with CTRL-C
    while True :

        sys.stdout.write("\r" + str(switch.state()))
        sys.stdout.flush()
        
        # Wait for 10 milliseconds
        time.sleep(0.1)

except KeyboardInterrupt :
    print("  Quit")
    # Reset GPIO settings
    GPIO.cleanup()