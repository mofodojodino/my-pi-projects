#!/usr/bin/python
#
# pir_test.py
# PIR test
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
import datetime
import time
import sys
import pdb

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

class PIRSensor :

    def __init__(self, pin, daylightOnly) :
        self.pin = pin
        self.daylightOnly = daylightOnly
        self.setup()
        print("Sensor " + str(pin))
        
    #
    # Setup pin as a input sensor
    #
    def setup(self) :
        GPIO.setup(self.pin, GPIO.IN, GPIO.PUD_UP)

    #
    # get current state of sensor
    #
    def state(self) :
        # pdb.set_trace()
        return GPIO.input(self.pin)

    #
    # test if sensor is active
    #
    def isActive(self) :
        if self.daylightOnly == True :
            day = Day()
            if day.isDaytime() == False :
                return False 
            
        return True if self.state() == GPIO.LOW else False

try : 
    
    pir = PIRSensor(4, False)
    
    sys.stdout.write("Waiting for PIRs to settle...\n")

    # Loop until PIR output is 0
    while pir.isActive() :
        sys.stdout.write("\rWaiting")
        sys.stdout.flush()
        
    sys.stdout.write("pir\n")
    
    # Loop until users quits with CTRL-C
    while True :

        sys.stdout.write("\r" + str(pir.state()))
        sys.stdout.flush()
        
        # Wait for 10 milliseconds
        time.sleep(0.1)

except KeyboardInterrupt :
    print("  Quit")
    # Reset GPIO settings
    GPIO.cleanup()