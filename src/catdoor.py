#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# catdoor.py
# Open and close a cat door using PIRs based
# on sunrise and sunset times.
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
from RPIO import PWM
import datetime
from astral import Astral
from pytz import timezone
import pytz
import time
import sys
import pdb

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)

class Day :
    region = 'Australia'
    city   = 'Sydney'

    #
    # Check if time is between sunrise and sunset
    #
    def isDaytime(self) :
        utc = pytz.utc
    
        a = Astral()
        a.solar_depression = 'civil'
    
        local = timezone(self.region + '/' + self.city)
        date = datetime.datetime.now()
        utcDate = utc.localize(date)
        city = a[self.city]
        sunrise = city.sunrise(date, local=False)
        sunset  = city.sunset(date, local=False)
    
        IsDaytime = False
        if sunrise < utcDate and sunset > utcDate:
            IsDaytime = True
    
        return IsDaytime

class PIRSensor :

    def __init__(self, pin, daylightOnly) :
        self.pin = pin
        self.daylightOnly = daylightOnly
        self.setup()
        print("Sensor " + str(pin))
        
    #
    # Setup pin as a input sensor
    #
    def setup(self) :
        GPIO.setup(self.pin, GPIO.IN, GPIO.PUD_UP)

    #
    # get current state of sensor
    #
    def state(self) :
        # pdb.set_trace()
        return GPIO.input(self.pin)

    #
    # test if sensor is active
    #
    def isActive(self) :
        if self.daylightOnly == True :
            day = Day()
            if day.isDaytime() == False :
                return False 
            
        return True if self.state() == GPIO.LOW else False
        
        
class Switch :

    def __init__(self, pin) :
        self.pin = pin
        self.setup()
        print("Switch on GPIO pin " + str(pin))
        
    #
    # Setup pin as a input sensor
    #
    def setup(self) :
        GPIO.setup(self.pin, GPIO.IN, GPIO.PUD_UP)

    #
    # get current state of sensor
    #
    def state(self) :
        # pdb.set_trace()
        return GPIO.input(self.pin)

    #
    # test if sensor is active
    #
    def isActive(self) :
        return True if self.state() == GPIO.LOW else False


class Door :
    
    def __init__(self, sensors, servo, servoPin, openedSwitch, closedSwitch) :
        self.sensors = sensors
        self.servo = servo
        self.servoPin = servoPin
        self.openedSwitch = openedSwitch
        self.closedSwitch = closedSwitch
        
        # lets set the door to closed position
        self.close()
        
    def open(self) :
        # Values for continuous rotation taken from 
        # http://www.active-robots.com/hitec-hs-785hb-winch-servo
        #
        # Target Positions between 2670µs and 2730µs will produce clockwise 
        # rotation in increasing velocity and positions between 550µs to 350µs 
        # will produce counter-clockwise rotation in increasing velocity.
        self.servo.set_servo(self.servoPin, 2730)
        
        # Run servo until the top or bottom switch is triggered
        self.waitForSwitchActivation()
        # Stop servo
        self.servo.stop_servo(self.servoPin)
        
        # make sure door stays open while there is movement
        while self.activeSensors() :
            time.sleep(0.01)
                    
        # door needs to be closed
        self.close()
    
    def close(self) :
        # Set to neutral position (1500µs) allowing 4 rotations from neutral either 
        # clockwise or anti-clockwise
        self.servo.set_servo(self.servoPin, 1500)
        # Run servo until the top or bottom switch is triggered
        self.waitForSwitchActivation()
        self.servo.stop_servo(self.servoPin)
                
        self.idle()
        
    def idle(self) :
        # stay idle for 5 seconds after activity
        time.sleep(5)
        
    def activeSensors(self) :
        
        # if any sensor is active return true
        counter = 0
        while counter < 10 :
            for sensor in self.sensors :
                if sensor.isActive() :
                    return True
            counter += 1
        
        # if we reach here no sensors are active
        return False
        
    def waitForSwitchActivation(self) :
        
        timeElapsed = 0
        timeStart = time.clock()
        # loop until a switch has been triggered or we have reached 5 secs 
        # as the maximum time allowed to move the door
        while self.openedSwitch.isActive() == False and self.closedSwitch.isActive() == False and timeElapsed < 5 :
            timeElapsed = time.clock() - timeStart
            

sys.stdout.write("PIR Module Test (CTRL-C to exit)\n")

# Define GPIO to use on Pi
ExternalSensorPin = 23
InternalSensorPin = 24

# Servo PIN - PWM (Pulse Width Modulation)
DoorServoPin = 18

# Door extremity switches
OpenedSwitchPin = 17
ClosedSwitchPin = 22


# PIRs
ExternalSensor = PIRSensor(ExternalSensorPin, False)
InternalSensor = PIRSensor(InternalSensorPin, True)

# Servo
DoorServo = PWM.Servo()

OpenedSwitch = Switch(OpenedSwitchPin)
ClosedSwitch = Switch(OpenedSwitchPin)

# list of all movement sensors
Sensors = [ExternalSensor, InternalSensor]

Switches = [OpenedSwitch, ClosedSwitch]

door = Door(Sensors, DoorServo, DoorServoPin, OpenedSwitch, ClosedSwitch)

try :
    
    sys.stdout.write("Waiting for PIRs to settle...\n")

    # Loop until PIR output is 0
    while ExternalSensor.isActive() and InternalSensor.isActive() :
        sys.stdout.write("\rWaiting")
        sys.stdout.flush()

    sys.stdout.write("Ready\n\n")

    # Set the initial door state
    door.idle()

    sys.stdout.write("External\tInternal\n")
    
    # Loop until users quits with CTRL-C
    while True :

        str = ""
        if ExternalSensor.isActive() :
            str += " Active "
        else :
            str += "Inactive"
        if InternalSensor.isActive() :
            str += "\t Active "
        else :
            str += "\tInactive"
            
        sys.stdout.write("\r" + str)
        sys.stdout.flush()
        
        # open the door a cat wants to travel
        if ExternalSensor.isActive() or InternalSensor.isActive() :
            door.open()
            
        # Wait for 10 milliseconds
        time.sleep(0.1)

except KeyboardInterrupt :
    print("  Quit")
    # Reset GPIO settings
    GPIO.cleanup()