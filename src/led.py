#!/usr/bin/python
#
# led.py
# test to turn on LEDs
#
# Author : Dean Tedesco
# Date   : 19/02/2015

# Import required Python libraries
import RPi.GPIO as GPIO
import datetime
import time
import sys
import pdb

# Use BCM GPIO references
# instead of physical pin numbers
GPIO.setmode(GPIO.BCM)


class LEDLight :
    
    def __init__(self, pin) :
        self.pin = pin
        self.setup()
        
    #
    # Setup pin as an output
    #
    def setup(self) :
        GPIO.setup(self.pin, GPIO.OUT)

    #
    # turn LED on
    #
    def on(self) :
        GPIO.output(self.pin, True)
     
    # 
    # turn LED off
    #
    def off(self) :
        GPIO.output(self.pin, False)
        


sys.stdout.write("LED Module Test (CTRL-C to exit)\n")

GreenLEDPin   = 17
RedLEDPin     = 22
BlueLEDPin    = 27

# LEDs
GreenLED = LEDLight(GreenLEDPin)
RedLED   = LEDLight(RedLEDPin)
BlueLED  = LEDLight(BlueLEDPin)

try :
    
#    sys.stdout.write("Waiting for PIRs to settle...\n")
#
#    # Loop until PIR output is 0
#    while ExternalSensor.isActive() and InternalSensor.isActive() :
#        sys.stdout.write("\rWaiting")
#        sys.stdout.flush()
#
#    sys.stdout.write("Ready\n\n")
#
#    # Set the initial door state
#    door.idle()
#
#    sys.stdout.write("External\tInternal\n")
#    
#    # Loop until users quits with CTRL-C
#    while True :
#
#        str = ""
#        if ExternalSensor.isActive() :
#            str += " Active "
#        else :
#            str += "Inactive"
#        if InternalSensor.isActive() :
#            str += "\t Active "
#        else :
#            str += "\tInactive"
#            
#        sys.stdout.write("\r" + str)
#        sys.stdout.flush()
#        
#        # open the door a cat wants to travel
#        if ExternalSensor.isActive() or InternalSensor.isActive() :
#            door.open()
#            
#        # Wait for 10 milliseconds
#        time.sleep(0.1)

except KeyboardInterrupt :
    print("  Quit")
    # Reset GPIO settings
    GPIO.cleanup()